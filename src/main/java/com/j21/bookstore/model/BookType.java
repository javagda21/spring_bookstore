package com.j21.bookstore.model;

public enum BookType {
    ADVENTURE, ACTION, SCIENCE
}
