package com.j21.bookstore.model.dto;

import com.j21.bookstore.model.BookType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;

// data transfer object
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookDto {
    @NotEmpty
    private String myBookTitle;
    private int yearWhenBookPublished;
    private BookType bookType;
    private int numberOfPagesInBook;
}
