package com.j21.bookstore.model.dto;

import com.j21.bookstore.model.BookType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// data transfer object
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddBookToAuthorDto extends BookDto{
    private Long authorId;
}
