package com.j21.bookstore.service;

import com.j21.bookstore.model.Book;
import com.j21.bookstore.model.BookType;
import com.j21.bookstore.model.dto.BookDto;
import com.j21.bookstore.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service // component - bean / singleton / DI
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    // read
    public List<Book> getBooks() {
        return bookRepository.findAll();
    }

    // create
    public void addBook(BookDto b) {
        bookRepository.save(map(b));
    }

    private Book map(BookDto dto) {
        return new Book(null,
                dto.getMyBookTitle(),
                dto.getYearWhenBookPublished(),
                dto.getBookType(),
                dto.getNumberOfPagesInBook(),
                null);
    }

    public void remove(Long id) { // id to nie jest pozycja!!!!
        bookRepository.deleteById(id);
    }
}
