package com.j21.bookstore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping("/index")
    public String getIndexPage() {
        return "indexPage";
    }

    @GetMapping("/")
    public String getIndexPageRedirect() {
        return "redirect:/index";
    }
}
