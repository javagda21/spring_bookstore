package com.j21.bookstore.controller;

import com.j21.bookstore.model.Book;
import com.j21.bookstore.model.BookType;
import com.j21.bookstore.model.dto.BookDto;
import com.j21.bookstore.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BookController {
    @Autowired /*Pobiera z DI (dependency injection) obiekt ktory pasuje po nazwie lub typie*/
    private BookService bookService;

    @GetMapping("/book/list")
    public String getBookList(Model model) {
        model.addAttribute("bookList", bookService.getBooks());

        return "bookListPage"; /*zwracamy html o nazwie 'bookListPage'*/
    }

    @GetMapping("/book/add")
    public String addBookForm(BookDto dto, Model model) {
        // przesyłamy do widoku wszystkie typy książek (gatunki), żeby móc wybrać typ książki z listy rozwijanej
        model.addAttribute("bookTypes", BookType.values());
        model.addAttribute("dto", dto);

        return "bookFormPage";
    }

    @PostMapping("/book/add")
    public String addBook(BookDto dto) {
        bookService.addBook(dto);

        return "redirect:/book/list";
    }

    @GetMapping("/book/remove")
    public String removeBook(@RequestParam(name = "bookId") Long id) {
        bookService.remove(id);

        return "redirect:/book/list";
    }
}
